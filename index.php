<!DOCTYPE html>
<html lang="es-ES">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta http-equiv="X-UA-Compatible" content="ie=edge">
	<title>Vtelca · Sitio en Mantenimiento</title>

	<!-- Fontawesome - Biblioteca de íconos -->
	<link rel="stylesheet" href="extensiones/fontawesome-free-5.9.0-web/css/all.css">
	
	<!-- Hojas de estilos -->
	<link rel="stylesheet" href="css/vtelca.css">

	<!-- Favicon -->
	<link rel="icon" sizes="any" href="favicon.png">
</head>
<body>
	<!-- Colores provisionales -->
	<main class="main-flex">
		<header class="main-flex--item">
			<!-- Realizar una prueba -->
			<!-- <a href="#modal" id="enlace-prueba">Realizar una prueba</a> -->
			<ul class="red-social">
				<li><a target="_blank" class="twitter" href="https://twitter.com/fabricavtelca" title="@fabricavtelca" target="_blank"><i class="fab fa-twitter"></i></a></li>
				<li><a target="_blank" class="instagram" href="https://instagram.com/fabricavtelca" title="@fabricavtelca" target="_blank"><i class="fab fa-instagram"></i></a></li>
				<li><a target="_blank" class="facebook" href="https://www.facebook.com/fabricavtelca" title="@fabricavtelca" target="_blank"><i class="fab fa-facebook-f"></i></a></li>
				<li><a target="_blank" class="youtube" href="https://www.youtube.com/channel/UCrKYoZZ7SdU2W3f42mpiDzw?view_as=subscriber" target="_blank"><i class="fab fa-youtube"></i></a></li>
			</ul>

		</header>
		
		<div class="main-flex--content">
			<h1>Sitio en Mantenimiento</h1>
			<div class="imagen-aviso" id="imagen-aviso">
				<img src="images/aviso-imagen.svg" alt="Vtelca Vectorizado">
			</div>

			<h3>Venezolana de Telecomunicaciones, C.A.</h3>
		</div>

		<!-- Pie de página -->
		<footer class="footer main-flex--item">
			
			<!-- Rejillas -->
			<div class="grid">
				<!-- Dirección de la empresa -->
				<div class="grid--item centradoXY">
					<div>
						<p class="text--left">Av. Bolívar, Meseta de Guaranao, Zona Franca de Paraguaná, Calle de Servicio N° 4, Galpones 5-5 y 5-6</p>
						<p class="text--left">Punto Fijo, Estado Falcón</p>
					</div>
				</div>

				<!-- Contactos -->
				<div class="grid--item">
					<h3>Contáctanos:</h3>
					<ul class="menu">
						<li class="menu--item">
							<span>Teléfono:</span>
							<ul class="menu">
								<li><a href="tel:+582692482907"><i class="fas fa-phone-alt"></i>&nbsp;(0269) 248 2907</a></li>
							</ul>
						</li>
						
						<li class="menu--item">
							<span>Asuntos Públicos:</span>
							<ul class="menu">
								<li><a href="mailto:ap.vtelca@gmail.com"><i class="fas fa-envelope"></i> ap.vtelca@gmail.com</a></li>
							</ul>
						</li>

						<li class="menu--item">
							<span>Comercialización:</span>
							<ul class="menu">
								<li><a href="mailto:comercializacion.vtelca@gmail.com"><i class="fas fa-envelope"></i> comercializacion.vtelca@gmail.com</a></li>
							</ul>
						</li>
					</ul>
				</div>

				<div class="grid--item">
					<h3>Enlaces:</h3>
					<ul class="menu">
						<li class="menu--item"><a href="//web-empleados.vtelca.gob.ve" target="_blank"><i class="far fa-arrow-alt-circle-right"></i> web-empleados.vtelca.gob.ve</a></li>
					</ul>
				</div>
			</div>

			<!-- Separador -->
			<hr>

			<div class="flex-legal">
				<img src="images/gobierno.svg" alt="Gobierno Bolivariano">
				<div>
					<img src="images/vtelca.svg" alt="Logo Vtelca">
					<span class="fc--base">G-20008334-4</span>
				</div>
				<img src="images/corselca.svg" alt="Corselca">
			</div>
		</footer>

		<!-- Ventana Modal -->
		<div class="modal" id="modal">
			<div class="modal--item">
				<div class="volver"><a href="#">Volver</a></div>
			</div>

			<div class="modal--content" id="modal-content">
				
			</div>
		</div>
	</main>

	<!-- Script -->
	<script src="js/dlAjax.js"></script>
	<script src="js/script.js"></script>
</body>
</html>
(function (window, document) {
    function isElement ( objeto ) {
        return Object.prototype.toString( objeto ) === "[object Object]";
    }
    var imagenAviso = new dlAjax("modal-content");
    
    var enlacePruebas = document.querySelector("#enlace-prueba");
    if ( isElement (enlacePruebas) ) {

        enlacePruebas.onclick = function () {
            imagenAviso.get("images/aviso-imagen-plano.svg");
        }
    }

}(window, document));